TMPDIR=./target
BDIR=./target/build
TDIR=./target/bin
SDIR=./src
PDIR=./public

# Don't override the assembler or linker
AS ?= as
LD ?= ld
# Override the compiler. Why would you ever not use LLVM :-)
CC = clang

example1: example0
	$(CC) -g -z execstack -fno-stack-protector -o $(TDIR)/example1 $(SDIR)/example1.c

example0: preamble
	$(AS) -g --64 -o $(BDIR)/example0.o $(SDIR)/example0.s
	$(LD) -g -m elf_x86_64 -o $(TDIR)/example0 $(BDIR)/example0.o

preamble:
	mkdir -p $(SDIR)
	mkdir -p $(BDIR)
	mkdir -p $(TDIR)
	mkdir -p $(PDIR)

clean:
	rm -rf $(TMPDIR)
